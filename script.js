const mainContainer = document.getElementById("main-container");
const products = document.getElementById("products");
const brandBox = document.querySelector("#brand");
const price = document.querySelector("#price");
let mobiles = [
  {
    brand: "Redmi",
    title: "9A",
    price: 6799,
    clicked: false,
  },
  {
    brand: "Samsung",
    title: "Galaxy M01 Core",
    price: 6199,
    clicked: false,
  },
  {
    brand: "OPPO",
    title: "A15",
    price: 9990,
    clicked: false,
  },
  {
    brand: "Redmi",
    title: "Note 9",
    price: 10999,
    clicked: false,
  },
  {
    brand: "Samsung",
    title: "Galaxy M31",
    price: 16499,
    clicked: false,
  },
  {
    brand: "OPPO",
    title: "A15s",
    price: 11490,
    clicked: false,
  },
  {
    brand: "Apple",
    title: "iphone 12 (64GB)",
    price: 48400,
    clicked: false,
  },
  {
    brand: "Apple",
    title: "iPhone 8",
    price: 45198,
    clicked: false,
  },
  {
    brand: "Redmi",
    title: "K20 Pro",
    price: 25380,
    clicked: false,
  },
];

brandBox.onclick = function (e) {
  if (e.target.value) {
    //check if checkbox is unselected
    if (!e.target.checked) {
      deleteDiv(e.target.value);
    } else {
      createDiv(e.target.value);
    }
  }
};

price.onclick = function (e) {
  if (e.target.value) {
    // divsPresentInDOM contains divs present in DOM
    let divsPresentInDOM = mobiles.filter((mobile) => {
      return mobile.clicked;
    });

    //Checking if dom is empty or not.
    if (divsPresentInDOM.length > 0) {
      checkAndCreateDivs(e.target.value);
    } else {
      // Dom is empty
      createDivByPrice(e.target.value);
    }

    //if checkbox is unselected
    if (!e.target.checked) {
      location.reload();
    }
  }
};

/**
 * creating divs for checked brand name
 * @param {*} brand :brand name of event listener
 */
function createDiv(brand) {
  for (let i = 0; i < mobiles.length; i++) {
    if (mobiles[i].brand.toLowerCase() === brand.toLowerCase()) {
      //making clicked true
      mobiles[i].clicked = true;
      let productDiv = document.createElement("div");
      productDiv.className = `product ${brand.toLowerCase()}`;
      productDiv.innerHTML = `<h2> ${mobiles[i].brand}</h2>
                  <br>
                  <h3>${mobiles[i].title}</h3>
                  <br>
                  <h3>${mobiles[i].price}</h3>`;
      products.appendChild(productDiv);
    }
  }
}

/**
 * deleting divs if checkbox is unchecked
 * @param {*} brand : brand name
 */
function deleteDiv(brand) {
  const pro = document.querySelectorAll(`.${brand.toLowerCase()}`);
  for (let i = 0; i < pro.length; i++) {
    pro[i].remove();
  }
  for (let i = 0; i < mobiles.length; i++) {
    if (mobiles[i].brand.toLowerCase() == brand.toLowerCase()) {
      mobiles[i].clicked = false;
    }
  }
}

/**
 * checking if existing divs are in given price range,if not deleting them.
 * @param {*} e event
 */
function checkAndCreateDivs(e) {
  let priceLow, priceHigh;
  if (e == "10000") {
    priceLow = 5000;
    priceHigh = 10000;
  } else if (e == "20000") {
    priceLow = 10000;
    priceHigh = 20000;
  } else if (e == "greater-20000") {
    priceLow = 20000;
    priceHigh = 50000;
  }

  for (let i = 0; i < mobiles.length; i++) {
    if (mobiles[i].clicked == true) {
      if (mobiles[i].price < priceLow || mobiles[i].price > priceHigh) {
        let productByClass = document.querySelectorAll(
          `.${mobiles[i].brand.toLowerCase()}`
        );
        for (let j = 0; j < productByClass.length; j++) {
          if (
            productByClass.item(j).lastElementChild.innerText < priceLow ||
            productByClass.item(j).lastElementChild.innerText > priceHigh
          ) {
            productByClass.item(j).remove();
          }
        }
      }
    }
  }
}
/**
 * Creating divs by price range.
 * @param {*} e event
 */
function createDivByPrice(e) {
  let priceLow, priceHigh;
  if (e == "10000") {
    priceLow = 5000;
    priceHigh = 10000;
  } else if (e == "20000") {
    priceLow = 10000;
    priceHigh = 20000;
  } else if (e == "greater-20000") {
    priceLow = 20000;
    priceHigh = 50000;
  }

  for (let i = 0; i < mobiles.length; i++) {
    if (mobiles[i].price >= priceLow && mobiles[i].price <= priceHigh) {
      mobiles[i].clicked = true;
      let productDiv = document.createElement("div");
      productDiv.className = `product ${mobiles[i].brand.toLowerCase()}`;
      productDiv.innerHTML = `<h2> ${mobiles[i].brand}</h2>
                    <br>
                    <h3>${mobiles[i].title}</h3>
                    <br>
                    <h3>${mobiles[i].price}</h3>`;
      products.appendChild(productDiv);
    }
  }
}
